## Python Tutorials

* [เริ่มต้น Python](python/about.md)
	* [ไวยกรณ์พื้นฐานที่จำเป็นอย่างยิ่งต้องจดจำ](python/important-basic-syntax.md)
	* [โครงสร้างของภาษา Python](python/python_structure.md)
	* [Library & Package ](python/Library&Package.md)
	* [Variable & Data Types (ตัวแปรและประเภทข้อมูล)](python/Variable&Data_Types.md)
	* [Lists](python/Lists.md)
	*  [Dictionary](python/Dictionary.md)
	* [Function (ฟังก์ชัน)](python/function.md)
	* [Modules](python/Modules.md)
	* [Classes & Objects](python/Classes&Objects.md)
	* [Inheritance](python/Inheritance.md)
	* [Datetime](python/Datetime.md)
	* [Awesome Python](python/Awesome_Python.md)


## Articles
### Programming

* **Python**

	*  [Learning Python: From Zero to Hero](articles/programming/learning-python-from-zero-to-hero.md)
	*  [Python Code Examples](articles/programming/python-code-example.md)
	*  [Python time Module](articles/programming/python-time-module.md)
	* [Python Functions and Functional Programming](articles/programming/Python_Functions_and_Functional_Programming.md)	
	* [Object Oriented Programming (OOP)](articles/programming/oop.md)
 
* **Pandas**
	* [Cheatsheet for Pandas](articles/programming/cheatsheet-pandas.md)
	* [21 Pandas operations for absolute beginners](articles/programming/21-pandas-operations.md)
	* [10 Pandas tips to make data analysis faster](articles/programming/10-pandas-tips-make-data-analysis-faster.md)
	* [Best of the API for Pandas](articles/programming/best-api-pandas.md)
	* [Minimally Sufficient Pandas](articles/programming/Minimally_Sufficient_Pandas.md)
	* [Selecting Subsets of Data in Pandas Part1](articles/programming/selecting-subsets-pandas-1.md)
	* [House Market Analysis (1/2)](articles/programming/pandas-house-market-analysis-1.md)
	* [House Market Analysis (2/2)](articles/programming/pandas-house-market-analysis-2.md)
	* [Python Pickle Module](articles/programming/python-pickle-module.md)


* **Flask**
	* [Single Page App with Flask and Vue.js](articles/programming/single-page-app-flask-vue.md)


* **Git**
	* [มาเรียนรู้ Git แบบง่ายๆกันเถอะ](articles/programming/getting_started_with_git.md)
	* [GitLab คืออะไร เริ่มใช้งานเบื้องต้น](articles/programming/getting_started_with_gitlab.md)
	* [A Step by Step Guide to Making Your First GitHub Contribution](articles/programming/First_GitHub.md)

* [**Link**]()
	* [GitBook_notebook](https://app.gitbook.com/@yo-sarawut/s/workspace/)
	* [Knowledge Base : GitHub](https://yosarawut.github.io/knowledge-base/)

### Lift


* [**Homo Sapiens**]()
	* [ตอนที่ 1 – กำเนิด Homo Sapiens](articles/lift/sapiens/sapiens-1.md)
	* [ตอนที่ 2 – สิ่งที่ทำให้เราครองโลก](articles/lift/sapiens/sapiens-2.md)
	* [ตอนที่ 3 – ยุคแห่งการล่าสัตว์เก็บพืชผล](articles/lift/sapiens/sapiens-3.md)
	* [ตอนที่ 4 – การหลอกลวงครั้งยิ่งใหญ่](articles/lift/sapiens/sapiens-4.md)
	* [ตอนที่ 5 – คุกที่มองไม่เห็น](articles/lift/sapiens/sapiens-5.md)
	* [ตอนที่ 6 – กำเนิดภาษาเขียน](articles/lift/sapiens/sapiens-6.md)
	* [ตอนที่ 7 – ความเหลื่อมล้ำ](articles/lift/sapiens/sapiens-7.md)
	* [ตอนที่ 8 – โลกที่ถูกหลอมรวม](articles/lift/sapiens/sapiens-8.md)
	* [ตอนที่ 9 – มนตราของเงินตรา](articles/lift/sapiens/sapiens-9.md)
	* [ตอนที่ 10 – จักรวรรดิ](articles/lift/sapiens/sapiens-10.md)
	* [ตอนที่ 11 – บทบาทของศาสนา](articles/lift/sapiens/sapiens-11.md)
	* [ตอนที่ 12 – ศาสนไร้พระเจ้า](articles/lift/sapiens/sapiens-12.md)
	* [ตอนที่ 13 – ยุคแห่งความไม่รู้](articles/lift/sapiens/sapiens-13.md)
	* [ตอนที่ 14 – 500 ปีแห่งความก้าวหน้า](articles/lift/sapiens/sapiens-14.md)
	* [ตอนที่ 15 – เมื่อยุโรปครองโลก](articles/lift/sapiens/sapiens-15.md)
	* [ตอนที่ 16 – สวัสดีทุนนิยม](articles/lift/sapiens/sapiens-16.md)
	* [ตอนที่ 17 – จานอลูมิเนียมของนโปเลียน](articles/lift/sapiens/sapiens-17.md)
	* [ตอนที่ 18 – ครอบครัวล่มสลาย](articles/lift/sapiens/sapiens-18.md)
	* [ตอนที่ 19 – สุขสมบ่มิสม](articles/lift/sapiens/sapiens-19.md)
	* [ตอนที่ 20 – อวสาน Sapiens](articles/lift/sapiens/sapiens-20.md)

* [Sapiens – A Brief History of Humankind (ประวัติย่อของมนุษยชาติ)](articles/lift/homo-sapiens.md)
* [โฮโม เซเปียนส์ สัตว์มหัศจรรย์และถิ่นที่อยู่](articles/lift/homosapien.md)
	
	

* [Homo Deus : A Brief History of Tomorrow](articles/lift/homo-deus.md)
* [**Homo Deus**]()
	* [Intro](articles/lift/homo-deus/intro.md)
	* [ตอนที่ 1 – สามวาระใหม่แห่งอนาคต](articles/lift/homo-deus/homo-deus-1.md)
	* [ตอนที่ 2 – คำสาปเรื่องดีอุส](articles/lift/homo-deus/homo-deus-2.md)
	* [ตอนที่ 3 – เซเปียนส์ครองโลกได้อย่างไร](articles/lift/homo-deus/homo-deus-3.md)
	* [ตอนที่ 4 – พลังของจิตวิสัยร่วม](articles/lift/homo-deus/homo-deus-4.md)
	* [ตอนที่ 5 – ข้อตกลงเรื่องความทันสมัยกับเทวทัณฑ์](articles/lift/homo-deus/homo-deus-5.md)
	* [ตอนที่ 6 – ปลายทางของการปฏิวัติมนุษย์นิยมคืออภิมนุษย์](articles/lift/homo-deus/homo-deus-6.md)
	* [ตอนที่ 7 – ไม่มีทั้งเจตจำนงเสรีและวิญญาณในโลกของข้อมูลนิยม](articles/lift/homo-deus/homo-deus-7.md)
	* [ตอนที่ 8 – เซเปียนส์กลายเป็นสิ่งชำรุดทางประวัติศาสตร์ได้อย่างไร](articles/lift/homo-deus/homo-deus-8.md)
	* [ตอนที่ 9 – มิจฉาทิฐิที่ร้ายแรงที่สุดในยุคขัอมูลนิยม](articles/lift/homo-deus/homo-deus-9.md)
	* [ตอนที่ 10 – พลังกุณฑาลินี](articles/lift/homo-deus/homo-deus-10.md)
	* [ตอนที่ 11 – ทฤษฎีแห่งสรรพสิ่ง ของลัทธิข้อมูลนิยมกับ​วิถีแห่งตัวตน](articles/lift/homo-deus/homo-deus-11.md)
	* [ตอนที่ 12 – เราต้องก้าวข้ามแต่หลอมรวมลัทธิข้อมูลนิยม](articles/lift/homo-deus/homo-deus-12.md)

* [SuperProductiveShow](articles/lift/super_productive.md)
* [ทำไม “ทัศนคติ” ถึงมีความสำคัญมากกว่า “ความฉลาดทางปัญญา (IQ)” ?](articles/lift/why-attitude-is-more-important-than-iq.md)

	
### Work
* [Brave New Work (ทางเลือกใหม่ของการบริหารจัดการทีม)](articles/work/brave-new-work.md)
* [15 ข้อคิดจาก คุณหมู วรวุฒิ CEO OF Officemate)](articles/work/15-comment-ceo-officemate.md) 

### Parent
* [การเรียนรู้ของลูกในวันนี้ ต้องมองที่โจทย์ของอนาคต](articles/parent/look-problems-of-the-future.md)
* [จะแก้ปัญหา เราจำเป็นต้องลงทุน จะกี่ขวบก็เริ่มต้นได้](articles/parent/parent-1.md)


### People
* [โซเชียลมีเดีย ในมุมมองของ มาร์ก ซักเคอร์เบิร์ก](articles/people/mark.md)
* [สรุป 5 บทเรียนธุรกิจจากหนังสือเจ้าสัวธนินท์ “ความสำเร็จ ดีใจได้วันเดียว”](articles/people/cp-book.md)

### Opinion
* [คุณรู้สึกว่า โลกทุกวันนี้หมุนเร็วและแคบลงหรือเปล่า?](articles/opinion/change.md)
* [ปัจจุบันเราต้องเผชิญกับความท้าทายอะไรบ้าง](articles/opinion/challenges.md)
* [บรรยง พงษ์พานิช: กับดักรัฐราชการ 4.0 ‘เพราะรัฐแสนเก่ง แสนดี ไม่มีจริง’](articles/opinion/banyong-government.md)
* [ปัญญาภิญโญ ณ Wongnai WeShare](articles/opinion/pinyo.md)
* [ประเทศไทยในความคิด ความคิดในประเทศไทย](articles/opinion/puey-talk16.md)
* [การสถาปนา ‘รัฐบรรษัทอำนาจนิยม’ ในสังคมไทย](articles/opinion/neo-corporatist-state.md)
 
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTE4NDM1NzY5ODEsLTE4MTU2MDg1MzgsMT
g5OTUyNzE2OCwxMDMwMzE0OTgzLC0xOTAwNzI4MDkxLC05Nzg0
ODcyMzUsMTQ5NTIwMzg1OSwtMTY4NjM4NTg5MCwtMTQ0MTg3Mz
g4NCwtMTg2Mzk1NzkyLDE4NTMxMjM3MjUsMTQ2NjcyNTYyNiwt
Mzc3MjE5NTY3LC05Njc5NjE1MDIsLTM5ODAyMDE1MiwtMzE4NT
c1MzUyLC01NTM5MjU5NDksMTgxMjc5NTE2LC05MTc1MDAyNTAs
MTM2NTMwMTMwOF19
-->