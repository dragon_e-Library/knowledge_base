Starting the project with Flask
==
## ตัวอย่าง 1
```
1. mkdir flask-tracking # 'flask-tracking' ชื่อ project

2. cd flask-tracking

3. virtualenv --no-site-packages venv

4. source venv/bin/activate
```
## ตัวอย่าง 2

1. create a project folder **"demo_flask"**
```
md demo_flask
```
```
cd demo_flask
```
2. create a virtual environment named `env`
```
python -m venv env
```
3. activate the environment
```
env\scripts\activate
```
4. Install Flask
```
pip install flask
```
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTE5NjA0MDgzMjddfQ==
-->