
# lift

[TOC]
* [Sapiens – A Brief History of Humankind (ประวัติย่อของมนุษยชาติ)](homo-sapiens.md)
* [โฮโม เซเปียนส์ สัตว์มหัศจรรย์และถิ่นที่อยู่](homosapien.md)
* [SuperProductiveShow](super_productive.md)
* [ทำไม “ทัศนคติ” ถึงมีความสำคัญมากกว่า “ความฉลาดทางปัญญา (IQ)” ?](why-attitude-is-more-important-than-iq.md)
<!--stackedit_data:
eyJoaXN0b3J5IjpbMTAwMDg3OTM5LC02NDUzMzgyNDBdfQ==
-->