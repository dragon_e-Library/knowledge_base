[สรุปหนังสือ] Homo Deus : A Brief History of Tomorrow
==
![enter image description here](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ_YRkaJoiNEE54Djnp4tkBT4g28_28QnVuXlE8W3UCbmqVY-4i)




“This is the best reason to learn history: not in order to predict the future, but to free yourself of the past and imagine alternative destinies. Of course this is not total freedom – we cannot avoid being shaped by the past. But some freedom is better than none.”

เมื่อเราเข้าใจ “เรื่องราวที่แท้จริง” ของมนุษย์ เมื่อนั้นเราก็จะเข้าใจ “เป้าหมาย” ของการเดินทางของมนุษยชาติ

Homo Deus คือ หนังสือภาคต่อจาก  [Sapiens](http://www.panasm.com/%E0%B8%AB%E0%B8%99%E0%B8%B1%E0%B8%87%E0%B8%AA%E0%B8%B7%E0%B8%AD-sapiens/)  ของ Yuval Noah Harari ศาสตราจารย์ทางประวัติศาสตร์ผู้นำเสนอเรื่องราวของ “อนาคต” ของมนุษยชาติผ่านการศึกษาความเป็นมาทางประวัติศาสตร์ของเผ่าพันธุ์ Homo Sapiens ที่พัฒนาตนเองขึ้นจากการเป็น “สัตว์” อันไร้ซึ่งความสำคัญใดๆมาเป็น “เทพเจ้า” ผู้กำหนดชะตาชีวิตของทุกสรรพสิ่ง แต่เมื่อสิ่งที่ถูกมนุษย์สร้างขึ้นอย่าง algorithm เริ่มมีสติปัญญาที่ชาญฉลาดกว่ามันสมองของมนุษย์ โลกของเราทุกคนจะเปลี่ยนแปลงไปอย่างไร
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTEwNjk0MDM5ODUsLTYzMTAxNjIwMl19
-->