Sapiens – A Brief History of Humankind (ประวัติย่อของมนุษยชาติ)
===
![enter image description here](https://images-na.ssl-images-amazon.com/images/I/41+lolL22gL._SX314_BO1,204,203,200_.jpg)

## Intro Homo Sapian

​หนังสือ **Sapiens** อธิบายว่าอะไรที่ทำให้ Homo sapiens หรือมนุษย์เผ่าพันธุ์เราประสบความสำเร็จบนโลกใบนี้  ทั้งที่ช่วงก่อนหน้า 70,000 ปีก่อน บรรพบุรุษของเราเป็นสัตว์ที่ “กระจอก” มาก กล่าวคือแทบไม่มีผลกระทบต่อสิ่งใดเลย อาศัยอยู่ในหลืบมุมของทวีปแอฟริกา แต่ในช่วงเวลา 70,000 ปีหลังที่ผ่านมา Homo sapiens ได้กระจายไปทวีปอื่นๆ และครองโลกใบนี้ในที่สุด ​Harari พบว่าเหตุที่ Homo sapiens ผงาดขึ้นมาได้ก็เพราะมนุษย์เป็นสัตว์ประเภทเดียวในโลกที่สามารถเชื่อในสิ่งซึ่งอยู่ในจินตนาการของเรา ไม่ว่าจะเป็นความเชื่อเรื่องพระเจ้า ความเป็นรัฐ สิทธิมนุษยชน ฯลฯ ตลอดจน “นิยาย” ที่ช่วยกันแต่งขึ้นมา และเชื่อกันเป็นตุเป็นตะ

นอกจากความสามารถที่จะเชื่อแล้ว มนุษย์ยังมีความสามารถพิเศษที่ไม่มีใครเหมือน นั่นคือ การใช้ความเชื่อและ “นิยาย” เหล่านี้มาทำให้สมาชิกเป็นหนึ่งเดียวกัน ตลอดจนสามารถจัดการกับคนจำนวนมากได้โดยได้รับความร่วมมือจากสมาชิกอีกด้วย ​มนุษย์เป็นสัตว์ชนิดเดียวที่มีจินตนาการ สามารถแต่งเรื่องขึ้นในสมอง และเชื่อเรื่องแต่งเหล่านั้นได้ เช่น หากทำชั่วจะตายไปแล้วตกนรก ไม่ว่านรกมีจริงหรือไม่ก็ตามแต่ ตราบที่มีคนเชื่อว่าจริง ก็สามารถนำมาใช้กำกับพฤติกรรมมนุษย์ได้ ในขณะที่ลิงชิมแปนซี ญาติที่ใกล้ชิดเราที่สุด ไม่สามารถคิดหรือจินตนาการอย่างนั้นได้ ​ความสามารถในการจัดการของมนุษย์ อันมีพื้นฐานมาจากการเชื่อ “นิยาย” เหล่านี้ร่วมกัน จนมนุษย์ยอมรับพระเจ้า ผีสางเทวดา กฎหมาย กฎกติกา ผู้นำ ฯลฯ ทำให้เราสามารถปราบคู่แข่งคือสมาชิกสกุล Homo ทั้งหลาย ตลอดจนสัตว์ร้าย และเอาชนะภัยพิบัติธรรมชาติ จนทำให้เราอยู่รอดมาได้อย่างดียิ่ง

**บทความสรุปหนังสือ Sapiens – A Brief History of Humankind**

- Sapiens ตอนที่ 1 – [กำเนิด Homo Sapiens](https://anontawong.com/2016/12/11/sapiens-1/)  
- Sapiens ตอนที่ 2 – [สิ่งที่ทำให้เราครองโลก  ](https://anontawong.com/2016/12/18/sapiens-2/)
- Sapiens ตอนที่ 3 – [ยุคแห่งการล่าสัตว์เก็บพืชผล  ](https://anontawong.com/2016/12/25/sapiens-3/)
- Sapiens ตอนที่ 4 – [การหลอกลวงครั้งยิ่งใหญ่  ](https://anontawong.com/2017/01/08/sapiens-4/)
- Sapiens ตอนที่ 5 – [คุกที่มองไม่เห็น  ](https://anontawong.com/2017/01/15/sapiens-5/)
- Sapiens ตอนที่ 6 – [กำเนิดภาษาเขียน  ](https://anontawong.com/2017/01/22/sapiens-6/)
- Sapiens ตอนที่ 7 – [ความเหลื่อมล้ำ  ](https://anontawong.com/2017/01/29/sapiens-7/)
- Sapiens ตอนที่ 8 – [โลกที่ถูกหลอมรวม  ](https://anontawong.com/2017/02/05/sapiens-8/)
- Sapiens ตอนที่ 9 – [มนตราของเงินตรา  ](https://anontawong.com/2017/02/12/sapiens-9/)
- Sapiens ตอนที่ 10 – [จักรวรรดิ  ](https://anontawong.com/2017/02/20/sapiens-10/)
- Sapiens ตอนที่ 11 – [บทบาทของศาสนา  ](https://anontawong.com/2017/02/26/sapiens-11/)
- Sapiens ตอนที่ 12 – [ศาสนไร้พระเจ้า  ](https://anontawong.com/2017/03/05/sapiens-12/)
- Sapiens ตอนที่ 13 – [ยุคแห่งความไม่รู้  
](https://anontawong.com/2017/03/19/sapiens-13/)
- Sapiens ตอนที่ 14 – [500 ปีแห่งความก้าวหน้า  ](https://anontawong.com/2017/03/26/sapiens-14/)
- Sapiens ตอนที่ 15 – [เมื่อยุโรปครองโลก  
](https://anontawong.com/2017/04/02/sapiens-15/)
- Sapiens ตอนที่ 16 – [สวัสดีทุนนิยม  
](https://anontawong.com/2017/04/09/sapiens-16/)
- Sapiens ตอนที่ 17 – [จานอลูมิเนียมของนโปเลียน  
](https://anontawong.com/2017/04/17/sapiens-17/)
- Sapiens ตอนที่ 18 – [ครอบครัวล่มสลาย  
](https://anontawong.com/2017/04/23/sapiens-18/)
- Sapiens ตอนที่ 19 – [สุขสมบ่มิสม  
](https://anontawong.com/2017/04/30/sapiens-19/)
- Sapiens ตอนที่ 20 – [อวสาน Sapiens](https://anontawong.com/2017/05/08/sapiens-20/)

## บทความที่เกี่ยวข้อง

- [“ลิง” พันธ์ุเดียวผู้ครองโลก](https://www.the101.world/homo-sapiens/)
- [สรุป Sapiens : A brief history of humankin](https://medium.com/@potaeeddylunna/%E0%B8%AA%E0%B8%A3%E0%B8%B8%E0%B8%9B-sapiens-a-brief-history-of-humankind-by-trader-group-%E0%B9%80%E0%B8%84%E0%B9%89%E0%B8%B2%E0%B9%80%E0%B8%AD%E0%B8%87-%E0%B8%81%E0%B9%87%E0%B9%80%E0%B8%84%E0%B9%89%E0%B8%B2%E0%B8%88%E0%B8%B0%E0%B8%95%E0%B8%B1%E0%B9%89%E0%B8%87%E0%B8%8A%E0%B8%B7%E0%B9%88%E0%B8%AD%E0%B9%81%E0%B8%9A%E0%B8%9A%E0%B8%99%E0%B8%B5%E0%B9%89%E0%B8%AD%E0%B9%88%E0%B8%B0-83dede47f875)
- [วิวัฒนาการของมนุษย์ : สารานุกรมไทยสำหรับเยาวชนฯ](http://kanchanapisek.or.th/kp6/sub/book/book.php?book=33&chap=4&page=chap4.htm)
- [Homo Deous บทที่ 1 เป้าหมายหลักต่อไปของมนุษยชาติคืออะไร ?](https://medium.com/@Trader4.0/%E0%B8%AA%E0%B8%A3%E0%B8%B8%E0%B8%9B-homo-deous-%E0%B8%9A%E0%B8%97%E0%B8%97%E0%B8%B5%E0%B9%88-1-%E0%B9%80%E0%B8%9B%E0%B9%89%E0%B8%B2%E0%B8%AB%E0%B8%A1%E0%B8%B2%E0%B8%A2%E0%B8%AB%E0%B8%A5%E0%B8%B1%E0%B8%81%E0%B8%95%E0%B9%88%E0%B8%AD%E0%B9%84%E0%B8%9B%E0%B8%82%E0%B8%AD%E0%B8%87%E0%B8%A1%E0%B8%99%E0%B8%B8%E0%B8%A9%E0%B8%A2%E0%B8%8A%E0%B8%B2%E0%B8%95%E0%B8%B4%E0%B8%84%E0%B8%B7%E0%B8%AD%E0%B8%AD%E0%B8%B0%E0%B9%84%E0%B8%A3-f98785ad0236)
<!--stackedit_data:
eyJoaXN0b3J5IjpbNjE2ODc4MDQwLC01MTY0Njg3NzMsLTE3MT
gwODE5NTYsLTMzMTc4NjQ0LC0xMTMwNDA2OTQ3LDU5MDcyOTIw
MF19
-->