โครงสร้างของภาษา Python
===

ในบทนี้ คุณจะได้เรียนรู้และทำความเข้าใจในโครงของภาษา Python ในภาษาคอมพิวเตอร์นั้นก็มีโครงสร้างของภาษาเช่นเดียวกกับภาษามนุษย์ ซึ่งสิ่งเหล่านี้ถูกกำหนดเพื่อเป็นรูปแบบและวิธีในการเขียนโปรแกรมในภาษา Python มันใช้สำหรับควบคุมวิธีที่คุณจะเขียนโค้ดของคุณเพื่อให้เข้าใจโดยตัวแปรภาษาหรือคอมไพเลอร์

## Simple Python program

เพื่อเริ่มต้นการเรียนรู้ในภาษา Python มาดูตัวอย่างของโปรแกรมอย่างง่าย โดยเป็นโปรแกรมที่ถามชื่อผู้ใช้และแสดงข้อความทักทายทางหน้าจอ มาเริ่มเขียนโปรแกรมแรกในภาษา Python ของคุณ ให้คัดลอกโปรแกรมข้างล่างแล้วนำไปรันใน IDE

```python
# My first Python program
name = input('What is your name?\n')
print ('Hi, %s.' % name)
print ('Welcome to Python.')
```

ในตัวอย่างเป็นโปรแกรมในการรับชื่อและแสดงข้อความทักทายออกทางหน้าจอ ในการรันโปรแกรมคุณสามารถรันได้หลายวิธี แต่ที่แนะนำคือการใช้ Python shell ให้คุณเปิด Python shell ขึ้นมาแล้วกดสร้างไฟล์ใหม่โดยไปที่  _File -> New File_  จะปรากฏกล่อง Text editor ของภาษา Python ขึ้นมา เพื่อรันโปรแกรม  _Run -> Run Module_  หรือกด  _F5_  โปรแกรมจะเปลี่ยนกลับไปยัง Python shell และเริ่มต้นทำงาน

![การรันโปรแกรมภาษา Python ใน Python Shell](http://marcuscode.com/media/68/marcuscode_eIk393k0_1000.jpg "Python program")

นี่เป็นผลลัพธ์การทำงานในการรันโปรแกรม first.py จาก Python shell ในตัวอย่างเราได้กรอกชื่อเป็น  _"Mateo"_  และหลังจากนั้นโปรแกรมได้แสดงข้อความทักทายและจบการทำงาน ในตอนนี้คุณยังไม่ต้องกังวลว่าโปรแกรมในแต่ละบรรทัดนั้นทำงานยังไง ซึ่งเราจะอธิบายในต่อไป

## Module

ในตัวอย่างโปรแกรมรับชื่อของเรา เป็นโปรแกรมแรกของเราในบทเรียน Python นี้ และเราได้บันทึกเป็นไฟล์ที่ชื่อว่า  _first.py_ ซึ่งไฟล์ของภาษา Python นั้นจะเรียกว่า Module ซึ่ง Module จะประกอบไปด้วยคลาส ฟังก์ชัน และตัวแปรต่างๆ และนอกจากนี้เรายังสามารถ import โมดูลอืนเข้ามาในโปรแกรมได้ ซึ่งโมดูลอาจจะอยู่ภายใน package ซึ่งเป็นเหมือน directory ของ Module ในตัวอย่าง  _first.py_ จึงเป็นโมดูลของโปรแกรมแรกของเรา

## Comment

คอมเมนต์ในภาษา Python นั้นเริ่มต้นด้วยเครื่องหมาย # คอมเมนต์สามารถเริ่มต้นที่ตำแหน่งแรกของบรรทัดและหลังจากนั้นจะประกอบไปด้วย Whilespace หรือโค้ดของโปรแกรม หรือคำอธิบาย ซึ่งโดยทั่วไปแล้วคอมเมนต์มักจะใช้สำหรับอธิบายซอสโค้ดที่เราเขียนขึ้นและมันไม่มีผลต่อการทำงานของโปรแกรม นี่เป็นตัวอย่างการคอมเมนต์ในภาษา Python

```python
# My first Python program

'''
This is a multiline comment 
'''

print ('Hello Python.') # Inline comment
```

ในตัวอย่าง เราได้คอมมเมนต์สามแบบด้วยกัน แบบแรกเป็นการคอมเมนต์แบบ single line แบบที่สองเป็นการคอมเมนต์แบบ multiline line และแบบสุดท้ายเป็นการคอมมเมนต์แบบ inline หรือการคอมเมนต์ภายในบรรทัดเดียวกัน

## Statement

Statement คือคำสั่งการทำงานของโปรแกรม แต่ละคำสั่งในภาษา Python นั้นจะแบ่งแยกด้วยการขึ้นบรรทัดใหม่ ซึ่งจะแตกต่างจากภาษา C และ Java ซึ่งใช้เครื่องหมายเซมิโคลอนสำหรับการจบคำสั่งการทำงาน แต่อย่างไรก็ตาม ในภาษา Python นั้นคุณสามารถมีหลายคำสั่งในบรรทัดเดียวกันได้โดยการใช้เครื่องหมายเซมิโคลอน ;

```python
name = input('What is your name?\n')
print ('Hi, %s.' % name);
print ('Welcome to Python.'); print ('Do you love it?')
```

ในตัวอย่าง เรามี 4 คำสั่งในโปรแกรม สองบรรทัดแรกเป็นคำสั่งที่ใช้บรรทัดใหม่ในการจบคำสั่ง ซึ่งเป็นแบบปกติในภาษา Python และบรรทัดสุดท้ายเรามีสองคำสั่งในบรรทัดเดียวที่คั่นด้วยเครืองหมาย ; สำหรับการจบคำสั่ง

## Indentation and while space

ในภาษา Python นั้นใช้ Whilespace และ Tab สำหรับกำหนดบล็อคของโปรแกรม เช่น คำสั่ง If Else For หรือการประกาศฟังก์ชัน ซึ่งคำสั่งเหล่านี้นั้นเป็นคำสั่งแบบบล็อค โดยจำนวนช่องว่างที่ใช้นั้นต้องเท่ากัน มาดูตัวอย่างของบล็อคคำสั่งในภาษา Python

```python
n = int(input ('Input an integer: '))

if (n > 0):
    print ('x is positive number')
    print ('Show number from 0 to %d' % (n - 1))

else:
    print ('x isn\'t positive number')

for i in range(n):
    print(i)
```

ในตัวอย่าง เป็นบล็อคของโปรแกรมจากท 3 คำสั่ง ในคำสั่งแรกคือ If ในบล็อคนี้มีสองคำสั่งย่อยอยู่ภายใน ที่หัวของบล็อคนั้นจะต้องมีเครื่องหมาย : กำหนดหลังคำสั่งในการเริ่มต้นบล็อคเสมอ อีกสองบล็อคสุดท้ายนั้นเป็นคำสั่ง Else และ For ซึ่งมีหนึ่งคำสั่งย่อยอยู่ภายใน ในภาษา Python นี้เข้มงวดกับช่องว่างภายในบล็อคมาก นั้นหมายความว่าทุกคำสั่งย่อยภายในบล็อคนั้นต้องมีจำนวนช่องว่างเท่ากันเสมอ

```python
n = int(input ('Input an integer: '))

# Invalid indent
if (n > 0):
    print ('x is positive number')
        print ('Show number from 0 to %d' % (n - 1))

# Valid indent
else:
    print ('x isn\'t positive number')

# Valid indent
for i in range(n):
        print(i)

```

นี่เป็นตัวอย่างการใช้งานช่องว่างที่ถูกต้องและไม่ถูกต้องภานในบล็อค ใสคำสั่ง If นั้นไม่ถูกเพราะทั้งสองคำสั่งมีจำนวนช่องว่างที่ไม่เท่ากัน สำหรับในคำสั่ง Else และ For นั้นถูกต้อง

## Literals

ในการเขียนโปรแกรม Literal คือเครื่องหมายที่ใช้แสดงค่าของค่าคงที่ในโปรแกรม ในภาษา Python นั้นมี Literal ของข้อมูลประเภทต่างๆ เช่น Integer Floating-point number และ String หรือแม้กระทั่งตัวอักษรและ boolean นี่เป็นตัวอย่างของการกำหนด Literal ให้กับตัวแปรในภาษา Python

```python
a = 1
b = -1.64E3
c = True
d = "marcuscode.com"
e = 'A'
```

ในตัวอย่าง เป็นการกำหนด Literal ประเภทต่างๆ ให้กับตัวแปร ในค่าที่เป็นแบบตัวเลขนั้นสามารถกำหนดค่าลงไปโดยตรงได้ทันทีและสามารถกำหนดในรูปแบบสั้นได้อย่างในตัวแปร b และสำหรับ boolean นั้นจะเป็น True ส่วน String หรือ Character นั้นจะต้องอยู่ภายในเครื่องหมาย double quote หรือ single quote เสมอ

## Expressions

Expression คือการทำงานร่วมกันระหว่างค่าตั้งแต่หนึ่งไปจนถึงหลายค่า โดยค่าเหล่านี้จะมีตัวดำเนินการสำหรับควบคุมการทำงาน ในภาษา Python นั้น Expression จะมีสองแบบคือ Boolean expression เป็นการกระทำกันของตัวแปรและตัวดำเนินการและจะได้ผลลัพธ์เป็นค่า Boolean โดยทั่วไปแล้วมักจะเป็นตัวดำเนินการเปรียบเทียบค่าและตัวดำเนินการตรรกศาสตร์ และ Expression ทางคณิตศาสตร์ คือการกระทำกันกับตัวดำเนินการและได้ค่าใหม่ที่ไม่ใช้ Boolean นี่เป็นตัวอย่างของ Expressions ในภาษา Python

```python
a = 4
b = 5

# Boolean expressions
print(a == 4)
print(a == 5)
print(a == 4 and b == 5)
print(a == 4 and b == 8)

# Non-boolean expressions
print(a + b)
print(a + 2)
print(a * b)
print(((a * a) + (b * b)) / 2)
print("Python " + "Language")
```

ในตัวอย่าง เรามีตัวแปร a และ b และกำหนดค่าให้กับตัวแปรเหล่านี้และทำงานกับตัวดำเนินการประเภทต่างๆ ที่แสดง Expression ในรูปแบบของ Boolean expression ที่จะได้ผลลัพธ์สุดท้ายเป็นเพียงค่า True และ False เท่านั้น ส่วน Non-Boolean expression นั้นสามารถเป็นค่าใดๆ ที่ไม่ใช่ Boolean

```result
True
False
True
False
9
6
20
20.5
Python Language
```



## Keywords

Keyword เป็นคำที่ถูกสงวนไว้ในการเขียนโปรแกรมภาษา Python เราไม่สามารถใช้คำสั่งเหล่านี้ในการตั้งชื่อตัวแปร ชื่อฟังก์ชัน คลาส หรือ identifier ใดๆ ที่กำหนดขึ้นโดยโปรแกรมเมอร์ นี่เป็นรายการของ Keyword ในภาษา Python

![enter image description here](https://gitlab.com/e_libraly/Python_Tutorial/raw/master/python/img/python_structures.jpg?inline=false)

ในบทนี้ คุณได้เรียนรู้เกี่ยวกับโครงสร้างของภาษา Python สิ่งเหล่านี้เป็นข้อกำหนดหรือกฎเกณฑ์ที่จำเป็นต้องใช้ในการเขียนโปรแกรม ซึ่งมันจะปรากฏในทุกๆ โปรแกรมที่คุณเขียน

Reference :
- [http://marcuscode.com/lang/python/program-struct](http://marcuscode.com/lang/python/program-struct)
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTE4NzQwMjAwMjFdfQ==
-->